# Default Flask 
from flask import Flask, request

import subprocess

# Blueprint - import sub_folder
from sub_aws.main_aws import app_aws

app = Flask(__name__)

# Blueprint - register sub_folder
app.register_blueprint(app_aws, url_prefix='/_aws')

@app.route('/') 
def default_index(): 
    return 'ok'

# before call the aws cli function, need to login first
# full url - http://127.0.0.1:5001/login?_id=xxxxx&_key=xxxxx
@app.route('/login')
def aws_configure_login():
    # (i.e. ?user=some-value)
    _id = request.args.get('_id')
    _key = request.args.get('_key').replace(" ", "+") # request.args.get will miss +
    p =subprocess.Popen(['aws', 'configure'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    # in this case, aws configure will request 4 times input, so each input needs to use the "\n" to end it ( just like a press Enter )
    enter_account = p.communicate(input=f'{_id}\n{_key}\n\njson\n'.encode())
    return 'login the aws cli success'