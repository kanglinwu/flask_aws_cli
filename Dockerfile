FROM python:3.9

# point to current work folder
WORKDIR /code

# Copy core file to the container folder
COPY main.py Pipfile Pipfile.lock .flaskenv /code/

# Install & use pipenv
RUN python -m pip install --upgrade pip
RUN apt-get update && apt-get install -y --no-install-recommends gcc

# install aws prepare
RUN apt-get install -y unzip \
    curl \
    && apt-get clean \
    && curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install \
    && rm -rf \
        awscliv2.zip \
    && apt-get -y purge curl \
    && apt-get -y purge unzip 

# export the pipfile to requirement.txt
RUN pip install pipenv 
RUN pipenv requirements --hash > requirements.txt 
RUN pip install -r requirements.txt 

CMD flask run