# flask_aws_cli

## How to use:
1. download this git repo:
```Shell
git clone https://gitlab.com/kanglinwu/flask_aws_cli.git
```
2. read the main.py and sub_aws/main_aws.py first
3. build the docker image by yourself or pull from public<br>
build image by yourself: refer to _#docker-file_<br>
pull image from public: refer to _#docker-image_<br>
4. run the docker container: <br>
refer to _#docker-container_


## Details:
### Flask
- [x] Add the BluePrint routing let user does not need to adjust the routing then need to re-build the docker image again.
> On main.py
> 1. Import the Blueprint.function. <br>
    _:7_
> 2. Register Blueprint. <br>
    _:12_

> On main_aws.py
>   1. Import the flask.Blueprint module. <br>
    _:1_
>   2. routing function change to Blueprint function<br>
    _:9_<br>
    _:21_
- [x] Add the one example routing to login the aws cli.<br>
`@app.route('/login')`
- [x] Add the one example routing to store the aws cli result to json file.<br>
`@app_aws.route('/get/ec2/describe-instances')`
- [x] Add the one example routing to return json file result<br>
`@app_aws.route('/display/ec2/describe-instances')`
### Docker file
- [x] Add the comment for each line
- [x] Standard operation process of docker build image
- check the current folder ( same poisition with Dockerfile )
```Shell
sudo docker build .
```
### Docker image
- [x] Provide the public docker image (docker hub)
```Shell
sudo docker pull kanglinwu/flask_aws_cli_v2
```
- [x] Re-tag the image<br>
- option value:<br>
_image-id, new-image-name, new-image-version_
- example:<br>
_sudo docker tag 37c73729eb12 download_from_internet_aws_cli:v1_
```Shell
sudo docker tag image-id new-image-name:new-image-version
```
### Docker container
- [x] run the docker<br>
- option value:<br>
_custom-http-port, local-static, local-sub_aws, image-name_
- example:<br>
_sudo docker run -p 5001:80 -v /home/kanglin/code/flask_aws_cli/static:/code/static -v /home/kanglin/code/flask_aws_cli/sub_aws:/code/sub_aws --name flask-aws -d kanglinwu/flask_aws_cli_v2_

```Shell
sudo docker run -p custom-http-port:80 -v local-static:/code/static -v local-sub_aws:/code/sub_aws --name flask-aws -d image-name
```
