from flask import Blueprint, jsonify

import json
import subprocess


app_aws = Blueprint('app_aws', __name__ )

@app_aws.route('/get/ec2/describe-instances')
def query_aws_get_ec2_describe_instances():
    # aws ec2 describe-instances --region=ap-northeast-1 --output=json
    result=subprocess.run(['aws', 'ec2', 'describe-instances', '--region=ap-northeast-1', '--output=json'], capture_output=True)
    result_json = result.stdout.decode('utf8').replace("'", '"')
    result_t_json = json.loads(result_json)
    result_to_json = json.dumps(result_t_json)
    # write the result ( json ) to into local file
    with open('static/aws_ec2_describe_instances.json', 'w') as file_object:
        file_object.write(result_to_json)
    return jsonify(result_t_json)

@app_aws.route('/display/ec2/describe-instances')
def display_ec2_describe_instances():
    # read the local json file and display to frontend
    with open('static/aws_ec2_describe_instances.json', 'r') as file_object:
        json_object = json.load(file_object)
    
    return jsonify(json_object)